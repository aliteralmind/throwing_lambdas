package com.github.fge.lambdas.suppliers;

import com.github.fge.lambdas.ThrowablesFactory;
import com.github.fge.lambdas.ThrowingFunctionalInterface;
import com.github.fge.lambdas.ThrownByLambdaException;

import java.util.function.Supplier;

/**
 * A throwing {@link Supplier}
 *
 * @param <T> type parameter of the return value of this supplier
 */
@FunctionalInterface
public interface ThrowingSupplier<T>
    extends Supplier<T>,
    ThrowingFunctionalInterface<ThrowingSupplier<T>, Supplier<T>>
{
    T doGet()
        throws Throwable;

    @Override
    default T get()
    {
        try {
            return doGet();
        } catch (Error | RuntimeException e) {
            throw e;
        } catch (Throwable tooBad) {
            throw new ThrownByLambdaException(tooBad);
        }
    }

    @Override
    default ThrowingSupplier<T> orTryWith(ThrowingSupplier<T> other)
    {
        return () -> {
            try {
                return doGet();
            } catch (Error | RuntimeException e) {
                throw e;
            } catch (Throwable ignored) {
                return other.doGet();
            }
        };
    }

    @Override
    default Supplier<T> fallbackTo(Supplier<T> fallback)
    {
        return () -> {
            try {
                return doGet();
            } catch (Error | RuntimeException e) {
                throw e;
            } catch (Throwable ignored) {
                return fallback.get();
            }
        };
    }

    default Supplier<T> orReturn(T defaultValue)
    {
        return () -> {
            try {
                return doGet();
            } catch (Error | RuntimeException e) {
                throw e;
            } catch (Throwable ignored) {
                return defaultValue;
            }
        };
    }

    @Override
    default <E extends RuntimeException> Supplier<T> orThrow(
        Class<E> exceptionClass)
    {
        return () -> {
            try {
                return doGet();
            } catch (Error | RuntimeException e) {
                throw e;
            } catch (Throwable tooBad) {
                throw ThrowablesFactory.INSTANCE.get(exceptionClass, tooBad);
            }
        };
    }
}
