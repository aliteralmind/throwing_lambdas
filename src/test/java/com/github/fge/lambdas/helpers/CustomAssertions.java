package com.github.fge.lambdas.helpers;

import org.assertj.core.api.Assertions;

public final class CustomAssertions
    extends Assertions
{
//TEMP FIX:
	@SuppressWarnings("unchecked")
    public static void shouldHaveThrown(final Class<? extends Throwable> c)
    {
//ORIGINAL:
//        failBecauseExceptionWasNotThrown(c);
//TEMP FIX:
    	failBecauseExceptionWasNotThrown((Class<? extends Exception>) c);
    }
}
